﻿using System;

namespace SingletonFixedIHope
{
    class Program
    {
        static void Main(string[] args)
        {
            Repo Create = Repo.GetInstance();
            Repo Read = Repo.GetInstance();
            Repo Update = Repo.GetInstance();
            Repo Delete = Repo.GetInstance();

            Create.Create();
            Read.Read();
            Update.Update();
            Delete.Delete();

            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
