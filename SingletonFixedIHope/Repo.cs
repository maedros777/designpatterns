﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonFixedIHope
{
    public class Repo
    {
        private Repo()
        {
        }

        public static Repo Instance;

        public static Repo GetInstance()
        {
            if (Instance == null )
            {
                return Instance = new Repo();
            }
            return Instance;
        }

        public void Create()
        {
            Console.WriteLine("Create");
        }
        public void Read()
        {
            Console.WriteLine("Read");
        }

        public void Update()
        {
            Console.WriteLine("Update");
        }

        public void Delete()
        {
            Console.WriteLine("Delete");
        }

    }
}
